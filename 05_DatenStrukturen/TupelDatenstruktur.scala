package com.rockthejvm.weitereDatenstrukturen

import java.time.LocalTime

object TupelDatenstruktur extends App {

  // Schreiben Sie eine WetterFunktion, welches beim Aufruf drei Werte zurückgibt:
  // eine Wetterbeschreibung (wie "sonnig", "regnerisch", etc.)
  // die aktuelle Zeit (verwenden Sie LocalTime)
  // die aktuelle Temperatur
  // Verwenden Sie dabei eine Tupel, um diese Werte zu retournieren.


  def getWeather(description: String, time: LocalTime, temperature: Double): (String, LocalTime, Double) = {
    (description, time, temperature)
  }

  val result: (String, LocalTime, Double) = getWeather("Bewölkt", LocalTime.now(), 13)

  println("Es ist: " + result._1)
  println("Die Zeit beträgt: " + result._2)
  println("Die Temperatur beträgt: " + result._3)

  //-----------------------------------------------------------------------------------

  // Erstellen Sie eine Liste mit Wetterdaten (wiederum als Tupel).
  // Implementieren Sie eine Funktion, welche die Daten ausliest und nach bestimmten Kriterien wieder ausgibt.
  // (z.Bsp. sollen alle Städte angezeigt werden, die wärmer als 20 Grad haben)

  val city1: (String, Double) = ("Zürich", 13)
  val city2: (String, Double) = ("New York", 4)
  val city3: (String, Double) = ("Bangkok", 28)

  val cityTemps = List(city1, city2, city3)

  def getWeatherOfCity(list: List[(String, Double)]): List[(String, Double)] = {
    val result = list.filter(x => x._2.>(20))
    result
  }

  println(getWeatherOfCity(cityTemps))

}
