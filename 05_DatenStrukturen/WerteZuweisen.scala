package com.rockthejvm.weitereDatenstrukturen

object WerteZuweisen extends App {

  // Gegeben ist folgende Map val m1: Map[String, String]. Wie lautet die Zuweisung?

  val m1: Map[String, String] =
    Map("key" -> "values")
  println(m1)

  // expected result = Map("key" -> "value")

  //-----------------------------------------------------------------------------------

  // Gegeben ist folgende Map val m2: Map[String, String].
  // Wir wollen die Map m1 aktualisieren mit einem neuen Wertepaar.

  val m2: Map[String, String] = m1.updated("key2", "value2")
  println(m2)

  // expected result = Map("key" -> "value", "key2" -> "value2")

  //-----------------------------------------------------------------------------------

  // Gegeben ist folgende Map val m3: Map[String, String].
  // Wir wollen die Map m2 aktualisieren mit einem neuen Wert.

  val m3: Map[String, String] = m2.updated("key2", "aDifferentValue")
  println(m3)

  // expected result = Map("key" -> "value", "key2" -> "aDifferentValue")

  //-----------------------------------------------------------------------------------

  // Gegeben ist die Map val m4: Map[String, String].
  // Wir wollen die Map m3 aktualisieren und den key entfernen.

  val m4: Map[String, String] = m3.removed("key")
  println(m4)

  // expected result = Map("key2" -> "aDifferentValue")

  //-----------------------------------------------------------------------------------

  // Wir wollen den Wert aus key von m3: val valueFromM3: Option[String]

  val valueFromM3: Option[String] = m3.get("key")
  println(valueFromM3)

  // expected result = Some("value")

  //-----------------------------------------------------------------------------------

  // Wir wollen den Wert aus key von m3: val valueFromM4: Option[String],
  // wobei wir somit keinen Wert erhalten, weil der key nicht existiert.

  val valueFromM4: Option[String] = m4.get("key")
  println(valueFromM4)

  // expected result = None

}
