package com.rockthejvm.functionsAsValues

object FlatMapExercises extends App {

  // Ihre Aufgabe ist es, eine neue Liste zu erstellen, die alle Elemente der Unterlisten enthält
  // wobei jede Zahl verdoppelt wird. Verwenden Sie dazu die flatMap-Funktion in Kombination mit map.

  // List(List(1, 2), List(3, 4), List(5, 6))

  val lists = List(List(1, 2), List(3, 4), List(5, 6))

  val result = lists.flatten
  println(result)

  //-----------------------------------------------------------------------------------

  // Gegeben ist eine Liste von Personen,
  // wobei jede Person durch ein Tupel aus Name und einer Liste ihrer Lieblingsfarben dargestellt wird:
  // List(("Max", List("Blau", "Grün")), ("Anna", List("Rot")), ("Julia", List("Gelb", "Blau", "Grün"))).
  // Ihre Aufgabe ist es, eine Liste aller einzigartigen Farben zu erstellen, die als Lieblingsfarben angegeben wurden.
  // Verwenden Sie dafür die flatMap-Funktion, gefolgt von distinct.

  val list = List(("Max", List("Blau", "Grün")), ("Anna", List("Rot")), ("Julia", List("Gelb", "Blau", "Grün")))

  val favColors = list.flatMap {
    case (name, colors) => colors.headOption.map(color => (name, color))
  }.distinct

  println(favColors)

}
