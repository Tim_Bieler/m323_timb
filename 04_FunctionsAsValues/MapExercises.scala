package com.rockthejvm.functionsAsValues

object MapExercises extends  App {

  // List(1, 2, 3, 4, 5)

  def mapMultiplication(list: List[Int]): List[Int] = {
    val result = list.map(x => x * 2)
    result
  }

  println(mapMultiplication(List(1, 2, 3, 4, 5)))
  // expected result = List(2, 4, 6, 8, 10)

  //-----------------------------------------------------------------------------------

  // List("Alice", "Bob", "Charlie")

  def mapNames(list: List[String]): List[String] = {
    val result = list.map(x => x.toUpperCase())
    result
  }

  println(mapNames(List("Alice", "Bob", "Charlie")))
  // expected result = List("ALICE", "BOB", "CHARLIE")

  //-----------------------------------------------------------------------------------

  // List(12, 45, 68, 100)

  def mapDivision(list: List[Float]): List[Float] = {
    val result = list.map(x => x / 2)
    result
  }

  println(mapDivision(List(12, 45, 68, 100)))
  // expected result = List(6, 22.5, 34, 50)

  //-----------------------------------------------------------------------------------

  case class Adresse(strasse: String, hausnummer: Int, postleitzahl: String, stadt: String)

  val adressen = List(
    Adresse("Hauptstrasse", 10, "12345", "Musterstadt"),
    Adresse("Nebenstrasse", 5, "23456", "Beispielburg")
  )

  val formatierteAdressen =
    adressen.map(x => "\"" + x.strasse + " " + x.hausnummer + ", " + x.postleitzahl + " " + x.stadt + "\"")
  println(formatierteAdressen)

  // expected result = List("Hauptstrasse 10, 12345 Musterstadt", "Nebenstrasse 5, 23456 Beispielburg")

  //-----------------------------------------------------------------------------------

  val namen = List("Max Mustermann", "Erika Mustermann")

  val vornamen =
    namen.map(x => x.replace(" Mustermann", "").toUpperCase())

  println(vornamen)

  // expected result = List("MAX", "ERIKA")
}
