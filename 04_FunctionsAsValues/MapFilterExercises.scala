package com.rockthejvm.functionsAsValues

object MapFilterExercises extends App {

  // Erstellen Sie eine Liste aller Mitarbeiter in der "IT"-Abteilung, deren Gehalt bei 50'000 oder darüber liegt.
  // Zusätzlich sollen alle Vornamen in Grossbuchstaben umgewandelt
  // und der Nachname entfernt werden (z.B. "MAX").

  case class Mitarbeiter(name: String, abteilung: String, gehalt: Int)

  val mitarbeiter = List(
    Mitarbeiter("Max Mustermann", "IT", 50000),
    Mitarbeiter("Erika Musterfrau", "Marketing", 45000),
    Mitarbeiter("Klaus Klein", "IT", 55000),
    Mitarbeiter("Julia Gross", "HR", 40000)
  )

  val result = mitarbeiter.filter(x => x.gehalt >= 50000).map(x => x.name.split(" ")(0).toUpperCase())
  println(result)

  // expected result = List("MAX", "KLAUS")

  //-----------------------------------------------------------------------------------

  val kurse = List(
    "Programmierung in Scala",
    "Datenbanken",
    "Webentwicklung mit JavaScript",
    "Algorithmen und Datenstrukturen"
  )

  // Filtern Sie die Kursnamen, um nur diejenigen zu behalten, die das Wort "Daten" enthalten.
  val first = kurse.filter(x => x.contains("Daten"))
  println(first)

  // Entfernen Sie aus jedem Kursnamen alle Leerzeichen.
  val second = kurse.map(x => x.replace(" ", ""))
  println(second)

  // Sortieren Sie die Kursnamen alphabetisch.
  val third = kurse.sorted
  println(third)

  // Sortieren Sie umgekehrt alphabetisch.
  val fourth = kurse.sorted.reverse
  println(fourth)
}
