package com.rockthejvm.functionsAsValues

object FoldLeft extends App {

  // List(1, 2, 3, 4, 5)
  // Berechnen Sie die Summe aller Zahlen mithilfe von foldLeft.

  def sumNumbers(list: List[Int]): Int = {
    val result = list.foldLeft(0)(_+_)
    result
  }

  println(sumNumbers(List(1, 2, 3, 4, 5)))

  // expected result = 15

  //-----------------------------------------------------------------------------------

  // List("Hallo", " ", "Welt", "!")

  val sentence = List("Hallo", " ", "Welt", "!")

  val helloWorld = sentence.foldLeft("")(_ + _)
  println(helloWorld)

  // expected result = "Hallo Welt!"

  //-----------------------------------------------------------------------------------

  //   Jeder Punkt wird durch ein Tupel (x, y) repräsentiert,
  //   wobei x und y die Koordinaten des Punktes sind. Ihre Aufgabe ist es,
  //   den Schwerpunkt (den durchschnittlichen Punkt) aller Punkte in der Liste zu berechnen.

  // val points = List((1, 3), (2, 5), (4, 8), (6, 2))

  val points = List((1, 3), (2, 5), (4, 8), (6, 2))
  val chords = points.foldLeft(0, 0) {
    case ((chordX, chordY), (x, y)) => (chordX + x, chordY + y)
  }
  val averageX = chords._1.toDouble / points.length
  val averageY = chords._2.toDouble / points.length

  val tuple = (averageX, averageY)

  println(tuple)

  // expected result = Tupel (x, y), das den Schwerpunkt aller Punkte in der Liste repräsentiert.
}
