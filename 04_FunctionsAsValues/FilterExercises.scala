package com.rockthejvm.functionsAsValues

object FilterExercises extends App {

  // List(1, 2, 3, 4, 5)

  def filterList(list: List[Int]): List[Int] = {
    val result = list.filter(x => x % 2 == 0)
    result
  }

  println(filterList(List(1, 2, 3, 4, 5)))
  // expected result = List(2, 4)

  //-----------------------------------------------------------------------------------

  // List("Alice", "Bob", "Charlie", "Diana")

  def filterNames(list: List[String]): List[String] = {
    val result = list.filter(x => x != "Diana")
    result
  }

  println(filterNames(List("Alice", "Bob", "Charlie", "Diana")))
  // List("Alice", "Charlie", "Diana")

  //-----------------------------------------------------------------------------------

  // List(12, 45, 68, 100)

  def overFifty(list: List[Int]): List[Int] = {
    val result = list.filter(x => x > 50)
    result
  }

  println(overFifty(List(12, 45, 68, 100)))
  // expected result = List(68, 100)

  //-----------------------------------------------------------------------------------

  // List("Scala", "ist", "fantastisch")

  def filterLetter_S(list: List[String]): List[String] = {
    val result = list.filter(x => x.startsWith("S"))
    result
  }

  println(filterLetter_S(List("Scala", "ist", "fantastisch")))

  // expected result = List("Scala")

  //-----------------------------------------------------------------------------------

  // Verwenden Sie filter und map, um eine Liste der Titel aller Bücher zu erstellen,
  // die vor 1950 veröffentlicht wurden.

  case class Buch(titel: String, autor: String, jahr: Int)

  val buecher = List(
    Buch("1984", "George Orwell", 1949),
    Buch("Brave New World", "Aldous Huxley", 1932),
    Buch("Fahrenheit 451", "Ray Bradbury", 1953)
  )

  val result = buecher.filter(x => x.jahr < 1950).map(x => "\"" + x.titel + "\"")
  println(result)

  // expected result = List("1984", "Brave New World")

  //-----------------------------------------------------------------------------------
}
