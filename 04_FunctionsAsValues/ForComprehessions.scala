package com.rockthejvm.functionsAsValues

object ForComprehessions extends App {

  // Schreiben Sie eine for-Comprehension,
  // die eine Liste von Ganzzahlen von 1 bis 10 durchläuft
  // und jede Zahl quadriert.
  // Tipp: Sie können Zahlenlisten ganz einfach so generieren: val numbers = 1 to 10

  val numbers = 1 to 10

  val quadNumb = for (
    num <- numbers
  ) yield num * num

  println(quadNumb)

  //-----------------------------------------------------------------------------------

  // Schreiben Sie eine for-Comprehension,
  // die eine Liste von Ganzzahlen von 1 bis 20 durchläuft
  // und nur die geraden Zahlen auswählt.

  val list = 1 to 20

  val straightNumb = for (
    i <- list
    if i % 2 == 0
  ) yield (i)

  println(straightNumb)

  //-----------------------------------------------------------------------------------

  // Gegeben sind zwei Listen von Strings: colors und fruits.
  // Schreiben Sie eine for-Comprehension,
  // die alle möglichen Paare aus einer Farbe und einer Frucht generiert.

  val colors = List("Red", "Green", "Blue")
  val fruits = List("Apple", "Banana", "Orange")

  val pairs = for {
    color <- colors
    fruit <- fruits
  } yield (color, fruit)

  println(pairs)

  //-----------------------------------------------------------------------------------

  // Gegeben sind zwei Listen von Benutzern (users) und deren Aufgaben (tasks).
  // Jeder Benutzer hat eine Liste von Aufgaben.
  // Schreiben Sie eine for-Comprehension, die alle Kombinationen von Benutzer und Aufgabe anzeigt,
  // aber nur diejenigen, bei denen die Aufgabe nicht abgeschlossen ist.

  case class User(name: String, tasks: List[String])

  val users = List(
    User("Alice", List("Task 1", "Task 2", "Task 3")),
    User("Bob", List("Task 1", "Task 4", "Task 5")),
    User("Charlie", List("Task 2", "Task 3", "Task 6"))
  )

  val tasksCompleted = List("Task 1", "Task 3", "Task 5")

  // TODO find pending tasks for each User

  val completedTasks = for {
    user <- users
    task <- user.tasks
    if !tasksCompleted.contains(task)
  } yield (user.name, task)

  println(completedTasks)

}
