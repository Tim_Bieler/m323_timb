package com.rockthejvm.pureFunctions

import scala.::

object Aufgabe03 extends App {

  // 3.1
  def recSum(list: List[Int], startNumb: Int, sum: Int): Int = {

    if (startNumb == list.size) {
      sum
    } else {
      val value = sum + list(startNumb)
      recSum(list, startNumb + 1, value)
    }
  }

  println("Summe einer Liste: " + recSum(List(1, 2, 3, 4), 0, 0))


  // 3.2
  def average(list: List[Int], startNumb: Int, sum: Int): Int = {

    if (startNumb == list.size) {
      sum / list.size
    } else {
      val value = sum + list(startNumb)
      average(list, startNumb + 1, value)
    }
  }

  println("MittelWert einer Liste: " + average(List(1, 2, 3, 4), 0, 0))


  // 3.3
  def alphabetical(list: List[String], sortedList: List[String]): List[String] = {

    if (!sortedList.isEmpty) {
      sortedList
    } else {
      val sortedList = list.sorted
      alphabetical(list, sortedList)
    }
  }

  println("Alphabetische Sortierung einer Liste: " +
    alphabetical(List("Nirvana", "Abba", "Gorillaz"), List[String]()))


  // 3.4

  /*
  Aufgabe 3.4:
    Eine Funktion, die eine Liste von Objekten anhand einer eigenen Sort-Funktion sortiert.
  X                                                                                                                 Die Objekte sollen dabei die Properties Datum, Priorität und Titel enthalten.
  Die Funktion soll eine sortierte Liste der Objekte zurückgeben,
  wobei als Sortierkriterium zuerst das Datum,
  dann die Priorität und zum Schluss der Titel verwendet werden.
  */

  object date
  object priority
  object title

  def sortObjects(list: List[Object], sortedList: List[Object], startNumb: Int): List[Object] = {

    if (sortedList.isEmpty || sortedList(0).equals(date) && sortedList(1).equals(priority)) {
      sortedList

    } else {

      if (list(startNumb).equals(date)) {
        sortedList.::(date)
      }
      if (list(startNumb).equals(priority)) {
        sortedList.::(priority)
      }

      sortObjects(list, sortedList, startNumb)
    }

  }

  println("" + sortObjects(List(title, date, priority), List[Object](), 0))

  // 3.5

}