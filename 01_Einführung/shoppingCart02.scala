package com.rockthejvm.excercises

object shoppingCart02 extends App {

    def getDiscountPercentage(warenkorb: List[String]): Int = {

      var percentage: Int = 0

      if (warenkorb.contains("Buch")) {
        percentage = percentage + 5
      }
      percentage
    }

    val warenkorb: List[String] = List("Buch", "oranges")
    println("Discount for Books: " + getDiscountPercentage(warenkorb) + "%")

}
