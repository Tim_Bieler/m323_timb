package com.rockthejvm.excercises

object TipCalculator03 extends App{

  def getTipPercentage(people: List[String]): Int = {

    var tipPercentage: Int = 0

    if (people.size > 5) {
      tipPercentage = tipPercentage + 20
    } else if (people.nonEmpty) {
      tipPercentage = tipPercentage + 10
    }

    tipPercentage
  }

  val people: List[String] = List("Tim", "Robin", "Adrian", "Castor", "Aaron", "Joseph Heeg")
  val person: List[String] = List("Tim")

  println("Tip for " + person + " = " + getTipPercentage(person))
  println("Tip for " + people + " = " + getTipPercentage(people))

}
