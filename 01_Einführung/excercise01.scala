package com.rockthejvm.excercises

object excercise01 extends App {

  def calculateScore(word: String): Int = {
    var score: Int = 0
    for (c: Char <- word.replace("a", "").toCharArray) {
      score = score + 1
    }
    score
  }

  println("calculateScore(\"imperative\") == " + calculateScore("imperative"))
  println("calculateScore(\"no\" == " + calculateScore("no"))


  def wordScore(word: String): Int = {
    word.replace("a", "").length()
  }

  println("wordScore(\"declarative\") == " + wordScore("declarative"))
  println("wordScore(\"yes\") == " + wordScore("yes"))

}
