package com.rockthejvm.anforderungen

/*
Wir planen eine Reise durch Europa und möchten,
dass der Benutzer jeweils die Destinationen für die Reise eingibt.
Zudem soll es möglich sein, eine bereits festgelegte Route zu ändern
(z.Bsp. wenn Ihr Freund noch einen anderen Zwischenaufenthalt möchte).
*/

object Reiseplan01 extends App {

  val routes = List("Paris", "Hamburg", "London")

  println("Reiseplan: " + routes)

  def planTrip(destination: String): List[String] = {
    val newList = routes :+ destination
    newList
  }

  println("Neuer Reiseplan: " + planTrip("Stockholm"))
}
