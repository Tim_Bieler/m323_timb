package com.rockthejvm.anforderungen

/*
Wir möchten eine App, welche für ein Auto-Rennen die gesamte Zeit für alle Runden berechnet.
Die App soll auch die Durchschnittszeit pro Auto berechnen.
Die erste Runde wird nicht mitgezählt, da es sich hier um eine "Warm-up" Runde handelt.
 */


object AutorennenSelf01 extends App{

  val car1AllRounds: Seq[Double] = Seq(44.2, 39.2, 40.1)
  val car1Rounds = car1AllRounds.tail
  val car2AllRounds: Seq[Double] = Seq(45.6, 36.1, 42.7)
  val car2Rounds = car2AllRounds.tail

  def getTotalTime(rounds: Seq[Double]): Double = {
    rounds.sum
  }

  def getSummarizedTime(rounds: Seq[Double]): Double = {
    val totalTime = rounds.sum
    totalTime / rounds.size
  }

  println("Rounds Driven by Car 1: " + car1Rounds)
  println("Total Time by Car 1: " + getTotalTime(car1Rounds))
  println("Summarized time by Car 1: " + getSummarizedTime(car1Rounds))

  println("Rounds Driven by Car 2: " + car2Rounds)
  println("Total Time by Car 2: " + getTotalTime(car2Rounds))
  println("Summarized time by Car 2: " + getSummarizedTime(car2Rounds))
}
